<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Shoe extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'name_shoe',
        'category',
        'gender',
        'qnt_available',
    ];
}
