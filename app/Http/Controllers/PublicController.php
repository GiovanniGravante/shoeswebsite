<?php

namespace App\Http\Controllers;

use App\Models\Shoe;
use Illuminate\Http\Request;

class PublicController extends Controller
{
    function homePage() {
        return view('welcome');
    }
    public function men() {
        $shoes = Shoe::all();
        return view('shop-men', compact('shoes'));
    }

    public function women() {
        $shoes = Shoe::all();
        return view('shop-women', compact('shoes'));
    }

    public function detailShoe(Shoe $shoe) {
        return view('detail_shoe', compact('shoe'));
    }

    // function popular() {
    //     return view('popular');
    // }

    // function trend() {
    //     return view('trend');
    // }







    
}
