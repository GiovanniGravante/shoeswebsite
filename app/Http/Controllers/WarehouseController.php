<?php

namespace App\Http\Controllers;

use App\Models\Shoe;
use Illuminate\Http\Request;

class WarehouseController extends Controller
{
    public function formShoes(){

        return view('warehouse.shoes-form');

    }

    public function postShoes(Request $request){
        // dd($request->all());
        // $shoes = new Shoe();
        // $shoes -> name_shoe = $request -> input('name_shoe');
        // $shoes -> category = $request ->input('category');
        // dd($shoes);
        // $shoes ->save();
        // return redirect(route('welcome'));




        $shoes = Shoe::create([
            'name_shoe' => $request -> input('name_shoe'),
            'category' => $request -> input('category'),
            'gender' => $request -> input('gender'),
            'qnt_available' => $request -> input('qnt_available'),
        ]
        );
        // dd($shoes);
        return redirect(route('home'));
    }
}
