<x-layout >
    <div class="py container">
        <h2 class='home__title-pages'>SCARPE UOMO</h2>

        <div class="card__container">
            @foreach ($shoes as $shoe)
                @if ($shoe->gender === 'MEN' || $shoe->gender === 'UNISEX')
                    <div class="card">
                        <img src="img/shoe-1.png" alt="Descrizione immagine" class="card-image">
                        <div class="card-content">
                            <h2 class="card-title">{{ $shoe->name_shoe }}</h2>
                            <p class="availability">Category: {{ $shoe->category }}</p>
                            {{-- <p class="availability">Quantity: {{ $shoe->qnt_available }} paia disponibili</p> --}}
                            <p class="availability">
                                @if ($shoe->qnt_available > 0)
                                Disponibile
                                @else
                                Non disponibile      
                                @endif                        
                            </p>
                            <button class="card__button">
                                <a href="{{route ('detailShoe', compact('shoe'))}}" class="card__text">detail</a>    
                            </button>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>
       
        
    </div>


</x-layout>