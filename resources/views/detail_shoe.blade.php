<x-layout>
    <div class="py container">
        <div class="card__container">
            
            <div class="card">
                <img src="/img/shoe-1.png" alt="Descrizione immagine" class="card-image">
                <div class="card-content">
                    <h2 class="card-title">{{ $shoe->name_shoe }}</h2>
                    <p class="availability">Category: {{ $shoe->category }}</p>
                    <p class="availability">
                        @if ($shoe->qnt_available > 0)
                        Disponibile
                        @else
                        Non disponibile      
                        @endif                        
                    </p>   
                    <button class="card__button">
                        <a class="card__text" href="{{route("home")}}"">back</a>
                    </button> 
                </div>
            </div>
            
        </div>
    </div>


</x-layout>