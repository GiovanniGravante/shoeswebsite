<header class="header" id="header">

    <nav class="nav container">
        <a href="/" class="nav__logo">
            <img src="/img/nike_logo.svg" alt="image">
        </a>

        <div class="nav__menu" id="nav-menu">
            
            <ul class="nav__list">
                <li class="nav__item">
                    <a href="{{route("uomini")}}" class="nav__link">MEN</a>
                </li>
            
                <li class="nav__item">
                    <a href="{{route("donne")}}" class="nav__link">WOMEN</a>
                </li>
               
                {{-- <li class="nav__item">
                    <a href="{{route("popolari")}}" class="nav__link">POPULAR</a>
                </li>
            
                <li class="nav__item">
                    <a href="{{route("moda")}}" class="nav__link">TREND</a>
                </li> --}}
                
                <li class="nav__item">
                    <a href="{{route("formShoes")}}" class="nav__link">MAGAZZINO</a>
                </li>
            </ul>

            {{-- CLOSE BUTTON --}}
            <div class="nav__close" id="nav-close">
                {{-- <i class="bi bi-toggle-on"></i> --}}
                <i class="bi bi-x-lg"></i>

            </div>

        </div>

        {{-- Toggle button --}}
        <div class="nav__toggle" id="nav-toggle">
            {{-- <i class="bi bi-toggle-off"></i> --}}
            <i class="bi bi-three-dots-vertical"></i>
        </div>


    </nav>

</header>
