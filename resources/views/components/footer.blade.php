<footer class="footer section">
    <div class="footer__container container grid">
        <a href="#" class="footer__logo">
            <img src="/img/nike_logo.svg" alt="">
        </a>

        <div class="footer__content">
            <h3 class="footer__title">Sections</h3>

            <ul class="footer__links">
                <li>
                    <a href="#specs" class="footer__link">MEN</a>
                </li>
                <li>
                    <a href="#case" class="footer__link">WOMEN</a>
                </li>
                <li>
                    <a href="#discount" class="footer__link">POPULAR</a>
                </li>
                <li>
                    <a href="#products" class="footer__link">TREND</a>
                </li>
            </ul>
        </div>

        <div class="footer__content">
            <h3 class="footer__title">Support</h3>

            <ul class="footer__links">
                <li>
                    <a href="#" class="footer__link">Product help</a>
                </li>
                <li>
                    <a href="#" class="footer__link">Register</a>
                </li>
                <li>
                    <a href="#" class="footer__link">Updates</a>
                </li>
                <li>
                    <a href="#" class="footer__link">Provides</a>
                </li>
            </ul>
        </div>

        <div class="footer__content">
            {{-- <form action="" class="footer__form">
                <input type="email" placeholder="Email" class="footer__input">
                <button class="button button--flex">
                    <i class="ri-send-plane-line button__icon"></i> Subscribe
                </button>
            </form> --}}

            <div class="footer__social">
                <a href="https://www.facebook.com/" target="_blank" class="home__social-link">
                    <i class="bi bi-facebook"></i>
                </a>
                <a href="https://www.instagram.com/" target="_blank" class="home__social-link">
                    <i class="bi bi-instagram"></i>
                </a>
                <a href="https://twitter.com/" target="_blank" class="home__social-link">
                    <i class="bi bi-youtube"></i>
                </a>
            </div>
        </div>
    </div>

    <p class="footer__copy">@GiovanniGravante. All right reserved </p>

</footer>