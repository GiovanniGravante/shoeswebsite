<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    {{-- FAVICON --}}
    <link rel="shortcut icon" href="img/favicon.png" type="image/x-icon">
    <!--=============== SWIPER CSS ===============-->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.css"/>    
    @vite(['resources/css/app.css','resources/js/app.js'])
 
   <title>ShoesWebsite</title>
    

</head>
<body>
    <x-header/>
    
    {{$slot}}

    <x-footer/>


     <!--=============== SWIPER JS ===============-->
     <script src="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.js"></script>
</body>
</html>