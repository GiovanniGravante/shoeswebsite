<x-layout>

    <main class="container_layout">
        <section class="home">
            <div class="home__container container">
                {{-- HOME-DATA --}}
                <div class="home__data">
                    <h2 class="home__subtitle">NIKE</h2>

                    <h1 class="home__title">
                        LUPINEK <br>
                        FLINIK <br>
                        ACG
                    </h1>

                    <p class="home__description">
                        We champion continual progress for athletes and sport by taking action to help athletes reach their potential. Every job at NIKE, Inc. is grounded in a team-first mindset, cultivating a culture of innovation and a shared purpose to leave an enduring impact.
                    </p>

                    <a href="#" class="home__button">BUY NOW</a>
                </div>
                
                {{-- HOME-IMAGE --}}
                <div class="home__images">

                    <div class="home__circle"></div>

                    <div class="home__swiper swiper ">
                        <div class="swiper-wrapper">
                            <article class="home__article swiper-slide">
                                <img src="img/shoe-1.png" alt="image-shoe" class="home__shoe">
                            </article>

                            <article class="home__article swiper-slide">
                                <img src="img/shoe-2.png" alt="image-shoe" class="home__shoe">
                            </article>

                            <article class="home__article swiper-slide">
                                <img src="img/shoe-3.png" alt="image-shoe" class="home__shoe">
                            </article>
                        </div>
                    </div>

                    {{-- Pagination --}}
                    <div class="swiper-pagination"></div>
                   
                    

                </div>

                {{-- HOME-SOCIAL --}}
                <div class="home__social">
                    <a href="https://www.facebook.com/nike/" target="_blank" class="home__social-link">
                        <i class="bi bi-facebook"></i>
                    </a>

                    <a href="https://www.instagram.com/nike/" target="_blank" class="home__social-link">
                        <i class="bi bi-instagram"></i>
                    </a>

                    <a href="https://www.youtube.com/@nike" target="_blank" class="home__social-link">
                        <i class="bi bi-youtube"></i>
                    </a>
                </div>

            </div>






        </section>

    </main>

</x-layout>