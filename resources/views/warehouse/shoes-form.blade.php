<x-layout>

<div class="py warehouse__container">
    <h1>ADD SHOES</h1>

    <form action="{{route('postShoes')}}" method="POST" enctype="multipart/form-data" class="form__container">
        @csrf
        <label class="label__form">NAME</label>
        <input class="input__form" type="text" name="name_shoe" placeholder="Pegasus,Air Jordan,...">

        {{-- <label class="label__form">Categoria</label>
        <input class="input__form" type="text" name="category" placeholder="Running,Soccer,..."> --}}

        <label class="label__form" for="category">CATEGORY</label>
        <input class="input__form" list="category-choice" name="category">
        <datalist id="category-choice">
            <option value="Running">
            <option value="Lifestyle">
            <option value="Basketball">
            <option value="Jordan">
            <option value="Calcio">
            <option value="Fitness">
        </datalist>

        {{-- <label class="label__form">Gender</label>
        <input class="input__form" type="text" name="gender" placeholder="Male,Female,Unisex"> --}}
        
        <label class="label__form" for="gender">GENDER</label>
        <input class="input__form" list="gender-choice" name="gender">
        <datalist id="gender-choice" class="datalist__gender">
            <option value="MEN">
            <option value="WOMEN">
            <option value="UNISEX">
        </datalist>

        <label class="label__form">QUANTITY</label>
        <input class="input__form" type="number" name="qnt_available" placeholder="Add qnt">
        <br>





        <br>
    
        <button class="btn__form" type="submit"> Add to warehouse </button>
    

    </form>

</div>








</x-layout>