<?php

use App\Http\Controllers\PublicController;
use App\Http\Controllers\WarehouseController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [PublicController::class, "homePage"])->name("home");

Route::get('/shop-men', [PublicController::class, "men"])->name("uomini");
Route::get('/shop-women', [PublicController::class, "women"])->name("donne");
Route::get('/shop-women/detail/{shoe}', [PublicController::class, "detailShoe"])->name("detailShoe");
// Route::get('/popular', [PublicController::class, "popular"])->name("popolari");
// Route::get('/trend', [PublicController::class, "trend"])->name("moda");

Route::get('/warehouse/shoes-form', [WarehouseController::class, "formShoes"])->name("formShoes");
Route::post('/warehouse/post', [WarehouseController::class, "postShoes"])->name("postShoes");
